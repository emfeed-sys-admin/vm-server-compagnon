#!/bin/bash

# This script gets a random available cow with `cowsay -l` and prints some text, default fortune

FORTUNE="/usr/games/fortune"
COWSAY="/usr/games/cowsay"

for COWNAME in `$COWSAY -l | tail -n +2`
do
COWS+=$COWNAME
COWS+='\n'
done
COWS=${COWS%??}
RANDOMCOW=$(echo -e $COWS | sort -R | head -n 1)
$FORTUNE | $COWSAY -f $RANDOMCOW
