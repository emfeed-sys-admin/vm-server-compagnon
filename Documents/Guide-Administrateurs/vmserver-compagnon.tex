\documentclass[a4paper, 12pt]{report}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[hyphens]{url}
\usepackage[pdfauthor = {{Arnaud Mounier}}, pdftitle = {{cador-emfeed compagnon}}, pdfstartview = Fit, pdfpagelayout = SinglePage, pdfnewwindow = true, bookmarksnumbered = true,breaklinks, colorlinks, linkcolor = red, urlcolor = black,citecolor = cyan, linktoc = all]{hyperref}
\usepackage{graphicx}
\usepackage{minted}

\newcommand{\vmserver}{\texttt{cador-emfeed}~}
\newcommand{\VM}{\texttt{VM}~}
\newcommand{\linux}{\texttt{GNU/Linux}~}
\newcommand{\debian}{\texttt{GNU/Linux Debian}~}
\newcommand{\ubuntu}{\texttt{GNU/Linux Ubuntu}~}
\newcommand{\conda}{\texttt{anaconda}~}
\newcommand{\mamba}{\texttt{mamba}~}
\newcommand{\vmadmin}{\texttt{vmadmin}~}
\newcommand{\R}{\texttt{R}~}
\newcommand{\python}{\texttt{python}~}
\newcommand{\nginx}{\texttt{nginx}~}
\newcommand{\jupyterhub}{\texttt{jupyterhub}~}
\newcommand{\RStudio}{\texttt{RStudio-serveur}~}
\newcommand{\biotools}{\texttt{biotools}~}

\graphicspath{{img/}}

\title{\texttt{cador-emfeed} Compagnon\newline Guide de l'administrateur pressé}
\author{\href{mailto:Arnaud Mounier <arnaud.mounier@cnrs.fr>}{Arnaud Mounier}}
\date{Mai 2022}
\begin{document}
   \maketitle
   {
    \hypersetup{hidelinks} % Sommaire en "noir"
    \renewcommand{\contentsname}{Sommaire} % Remplacer "Table des matières"*
    \tableofcontents % Affichage du sommaire
    }

\chapter{Administration du serveur \vmserver}
\section{Instalation du serveur}
\par A priori le serveur \vmserver~est déjà installé car la solution choisie est une machine virtuelle packagée sous \ubuntu~20.04.
Au début, il va donc s'agir de :
\begin{itemize}
   \item Monter le volume \texttt{NFS} à la place du \texttt{/home} initial.
   \item Configurer l'utilisateur administrateur du système : \vmadmin
   \begin{itemize}
      \item Recopier son compte utilisater du \texttt{/home} initial vers le nouveau \texttt{/home} du volume \texttt{NFS} en \textit{préservant les droits}.
   \end{itemize}
   \item Faire une mise à jour complète du système \ubuntu.
   \item Configurer les partitions logiques \texttt{LVM}.
\end{itemize}
\subsection{Instalation de la distribution \ubuntu}
\subsubsection{Configurer les volumes et partitions systèmes}

\par Note : nous considérons dans cette partie que les demandes Ariane ont été faites et appliquées par la DSI.

\par En premier lieu, il convient de monter avec les bons paramètres le volume distant \texttt{NFS}. Il s'agit simplement d'ajouter cette ligne dans le fichier \texttt{/etc/fstab}~:
\begin{minted}{bash}
147.100.166.34:/dijon_agroecologie_emfeed /home    nfs   defaults 0  0
\end{minted}
et de commenter à l'aide du caractère \texttt{\#} celle de l'ancien \texttt{/home} ainsi :
\begin{minted}{bash}
#/dev/mapper/vg1-home /home           ext4    defaults        0       0
\end{minted}
puis de remonter le système de fichier :
\begin{minted}{bash}
> sudo mount -a 
\end{minted}

\par Une fois ceci fait, il faut copier le répertoire \texttt{/home} de l'utilisateur \texttt{vmadmin} de l'ancienne partition vers le nouveau \texttt{/home} du volume \texttt{NFS}.
\begin{minted}{bash}
> sudo mkdir /mnt/old_home
> sudo mount /dev/mapper/vg1-home /mnt/old_home
> cp -rp /mnt/old_home/vmadmin /home 
> sudo umount /mnt/old_home
> sudo rmdir /mnt/old_home
\end{minted}
On peut alors déconnecter et reconnecter l'utilisateur \texttt{vmadmin} pour retrouver un environnement correct et faire la mise à jour complète du système.
\begin{minted}{bash}
> sudo apt update
> sudo apt full-upgrade
\end{minted}

\par Pour des raisons de sécurité, s'il le noyau \linux est mis à jour (le paquet \texttt{linux-image-x.y.z-XXX-generic} sera présent dans la liste), il faut impérativement redémarrer la \VM :
\begin{minted}{bash}
> sudo reboot
\end{minted}

\par Il est temps de créer les partitions nécessaires et d'attribuer correctement la mémoire aux partitions logiques. La \VM utilise le système \texttt{LVM} pour \textit{Logical Volume Manager}.
Au départ, les partitions ont une mémoire minimum, il convient donc de distribuer la mémoire libre correctement. Une documentation complète ainsi que les principes de partition logique peut être trouver sur ce site : \href{https://www.it-connect.fr/gestion-des-lvm-sous-linux}{https://www.it-connect.fr/gestion-des-lvm-sous-linux}.

\par Pour attribuer correctement la mémoire, on va s'inspirer de la mémoire définie sur le serveur physique \vmserver et laisser une marge de manoeuvre pour la suite. Le principale avantage de \texttt{LVM} est que les partitions peuvent être créée et la mémoire redistribuée \textit{à chaud} c'est à dire quand la \VM est active (pas besoin de la redémarrer donc).

\par Tout d'abord regardons la mémoire libre disponible sur le volume logique \texttt{vg1} :
\begin{minted}{bash}
vmadmin@vm-pkglinux-100:~$ sudo vgdisplay
[sudo] password for vmadmin: 
  --- Volume group ---
  VG Name               vg1
  System ID             
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  18
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                8
  Open LV               7
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <499,52 GiB
  PE Size               4,00 MiB
  Total PE              127877
  Alloc PE / Size       4764 / <18,61 GiB
  Free  PE / Size       123113 / 480,91 GiB
\end{minted}

Il y a \texttt{480 Go} de mémoire libre. On va d'abord créer une partition logique \texttt{/vol} pour accueillir les bases de données, avec cette suite de commande :
\begin{minted}{bash}
> sudo lvcreate -n vol -L 150g vg1
> sudo mkfs -t ext4 /dev/vg1/vol
> sudo mkdir /vol
\end{minted}

\par Puis on modifie le fichier \texttt{/etc/fstab} pour monter la partition au démarrage :
\begin{minted}{bash}
> sudo vim /etc/fstab
\end{minted}
et on ajoute cette ligne :
\begin{minted}{bash}
/dev/mapper/vg1-vol /vol           ext4    defaults        0       0
\end{minted}
et on remonte les partitions à partir du nouveau fichier ainsi modifié.
\begin{minted}{bash}
> sudo mount -a
\end{minted}

Voici la répartition initiale de la mémoire de la \VM lors de sa création :
\begin{minted}{bash}
Filesystem                                 Size  Used Avail Use% Mounted on
/dev/mapper/vg1-root                       5,4G  3,0G  2,2G  59% /
/dev/mapper/vg1-opt                        920M   44K  857M   1% /opt
/dev/mapper/vg1-var                        3,6G  1,2G  2,2G  36% /var
/dev/mapper/vg1-varlog                     1,8G  220M  1,5G  13% /var/log
/dev/mapper/vg1-varspool                   2,7G  132K  2,6G   1% /var/spool
/dev/mapper/vg1-tmp                        920M   68K  857M   1% /tmp
\end{minted}

On va maintenant répartir la mémoire libre en s'inspirant de l'ancien \vmserver physique, ce qui donne pour la \VM :
\begin{minted}{bash}
Filesystem                Size         Resize    
/dev/mapper/vg1-root      40Go         +  15Go
/dev/mapper/vg1-opt      200Go         + 200Go
/dev/mapper/vg1-var       15Go         +  11Go  
/dev/mapper/vg1-varlog     4Go         +   3Go                         
/dev/mapper/vg1-tmp        2Go         +   2Go
\end{minted}

\par La partition \texttt{/opt} va accueillir la distribution \conda, il faut lui faut donc de la place. Quand à la partition système \texttt{/var/spool} elle contient les données à exécuter dans la future, on ne va pas y toucher [NA: en fait, no sais trop à quoi elle sert...]. 

\par Voici les commandes pour la partion logique \texttt{/} (il faut impérativement effectuer ces deux commandes). Il faudra faire de même pour les autres en adaptant les quantités~:
\begin{minted}{bash}
> sudo lvresize -L +15g /dev/vg1/root
> sudo resize2fs /dev/vg1/root
\end{minted}

À la fin des manipulations vos partitions devraient être ainsi :
\begin{minted}{bash}
Filesystem                                 Size  Used Avail Use% Mounted on
/dev/sda1                                  462M  221M  213M  52% /boot
/dev/mapper/vg1-opt                        198G   44K  190G   1% /opt
/dev/mapper/vg1-tmp                        2,9G   68K  2,8G   1% /tmp
/dev/mapper/vg1-var                         15G  1,2G   13G   9% /var
/dev/mapper/vg1-varlog                     4,8G  220M  4,4G   5% /var/log
/dev/mapper/vg1-varspool                   2,7G  132K  2,6G   1% /var/spool
147.100.166.34:/dijon_agroecologie_emfeed  4,8T  1,7M  4,8T   1% /home
/dev/mapper/vg1-vol                        147G   28K  140G   1% /vol
\end{minted}

C'est bein le fun.

La mémoire disponible est de :
\begin{minted}{bash}
vmadmin@vm-pkglinux-100:~$ sudo vgdisplay
  --- Volume group ---
  VG Name               vg1
  Free  PE / Size       25577 / 99,91 GiB
\end{minted}
soit 100Go à repartir selon les besoins ultérieurs avec les commandes vues plus haut.

\subsubsection{Applications nécessaires}

\par Pour fonctionner correctement, le serveur a besoin d'un gestionnaire de version : git. Même s'il en existe d'autre, celui-ci est tellement répandu qu'il est devenu incontournable, ne serait-ce que pour 
accéder à la forge institutionnelle \texttt{forgemia}. Pour l'installer, c'est plutôt simple :
\begin{minted}{bash}
vmadmin@vm-pkglinux-100:~$ sudo apt install git
\end{minted}


\subsubsection{serveur https nginx}

\subsection{Instalation de la distribution \conda}
\par L'installation de la distribution \conda se fait en trois étapes :
\begin{itemize}
   \item installation de la distribution minimale \texttt{miniconda} ;
   \item configuration des \textit{channels} ;
   \item installation de \texttt{mamba} pour plus de stabilité.
\end{itemize}
Par la suite, il sera de bonne pratique de procéder à la création d'environnement virtuels qui vont isoler les différentes applications.
Après cette installation, le serveur \vmserver sera pourvu de 4 environnements virtuels \conda :
\begin{itemize}
   \item \texttt{jupyterhub-srv}~: environnement qui isole le fonctionnement de \texttt{JupyterHub}~;
   \item \texttt{biotools}~: environnement qui isole toutes les applications bio-informatiques installables depuis le \textit{channel} \texttt{bioconda}~;
   \item \texttt{vasa-legacy}~: environnement qui isole le \textit{workflow} \texttt{VASA/IlluminaMetabarcoding} version 1 sous \texttt{python 2.7}~;
   \item \texttt{vasa-asv}~: environnement qui isole le \textit{workflow} \texttt{VASA/ASV-workflow}.
\end{itemize}
Il est possible d'en ajouter à loisir.

\subsubsection{Installation de \texttt{miniconda}}

\par Il convient de suivre ce guide : \href{https://docs.conda.io/en/latest/miniconda.html}{https://docs.conda.io/en/latest/miniconda.html}. Une question se pose : quelle version de python choisir ?
Comme \ubuntu est en \texttt{python 3.8}, il semble sensé de resté cohérent et de choisir cette version. Attention, nous allons installé \conda dans le répertoire \texttt{/opt} de \vmserver, il conviendra donc de changer le propriétaire de cette partition et de mettre les bons droits en lecture, écriture et exécution. La deuxième ligne de code sert à vérifier l'intégrité du script d'installation, il faut comparer la sortie de la commande avec la ligne qui va bien dans ce tableau : \href{https://docs.conda.io/en/latest/miniconda_hashes.html}{hashes}. 

\begin{minted}{bash}
> sudo chown -R vmadmin:vmadmin /opt
> sudo chmod -R 755 /opt
> wget https://repo.anaconda.com/miniconda/Miniconda3-py38_4.12.0-Linux-x86_64.sh
> sha256sum Miniconda3-py38_4.12.0-Linux-x86_64.sh
> bash Miniconda3-py38_4.12.0-Linux-x86_64.sh
Répondre :
- Do you accept the license terms? [yes|no]
[no] >>> yes

- Miniconda3 will now be installed into this location:
/home/vmadmin/miniconda3

  - Press ENTER to confirm the location
  - Press CTRL-C to abort the installation
  - Or specify a different location below

[/home/vmadmin/miniconda3] >>> /opt/conda

- installation finished.
Do you wish the installer to initialize Miniconda3
by running conda init? [yes|no]
[no] >>> yes
\end{minted}

\par Une fois l'installation de \texttt{miniconda} terminé, il faut se déloguer et se reloguer. Si tout va bien, votre « prompt » devrait ressembler à ceci :
\begin{minted}{bash}
(base) vmadmin@vm-pkglinux-100:~$
\end{minted}

\par Il ne reste plus qu'à mettre à jour la distribution :
\begin{minted}{bash}
(base) vmadmin@vm-pkglinux-100:~$ conda update conda
\end{minted}

\par Puis, on installe \mamba, simplement parce que \mamba facilite pas mal la vie... \href{https://mamba.readthedocs.io/en/latest/installation.html}{Installation de \mamba}.
\begin{minted}{bash}
> conda install mamba -n base -c conda-forge
\end{minted}

\par Et finallement, on configure les \textit{channels} par défaut :
\begin{minted}{bash}
> conda config --add channels defaults
> conda config --add channels bioconda
> conda config --add channels conda-forge
\end{minted}


\subsection{Instalation du langage \R}

\subsubsection{Instalation du langage}

\par Pour avoir une version à jour de \R, il faut changer les sources de ce language dans la distribution \ubuntu, tout est expliqué \href{}{ici} :
\begin{minted}{bash}
# update indices
sudo apt update -qq
# install two helper packages we need
sudo apt install --no-install-recommends software-properties-common dirmngr
# add the signing key (by Michael Rutter) for these repos
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc 
# Fingerprint: E298A3A825C0D65DFD57CBB651716619E084DAB9
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# add the R 4.0 repo from CRAN -- adjust 'focal' to 'groovy' or 'bionic' as needed
sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
\end{minted}

\par Une fois ajouté la bonne source dans le répertoire \texttt{/etc/apt/source.list.d}, il ne reste plus qu'à installer les paquets de base de \R :
\begin{minted}{bash}
>sudo apt install --no-install-recommends r-base
\end{minted}

\par En option, il est possible de rajouter une autre source \R avec cette commande :
\begin{minted}{bash}
> sudo add-apt-repository ppa:c2d4u.team/c2d4u4.0+
\end{minted}
Cette option a été ajoutée lors de l'installation initiale. Pour la gestion ultérieur des paquets \R, il est préférable de suivre cette règle de bonne pratique :
\begin{itemize}
   \item Chercher dans les paquets \ubuntu si la librairie existe :
   \begin{minted}{bash}
   > sudo apt search mon_joli_paquet_R
   > sudo apt install r-mon_joli_paquet_R
   \end{minted}
   \item Sinon, installer avec la commande :
   \begin{minted}{bash}
   > sudo R
   > install.packages("mon_joli_paquet_R")
   # ou
   > install.packages(c("ma","jolie","liste","de","paquets","R"))
   \end{minted}
   \item Si toujours pas, installer via un dépot \texttt{git} ;
   \item Et si le paquet n'est plus maintenu et même si ce n'est pas une très bonne idée, il est possible de l'\href{https://www.listendata.com/2015/05/r-install-archived-package.html}{installer} en retrouvant l'archive la plus récente.
\end{itemize}

\subsubsection{Instalation de \RStudio}
\par Comme la version \texttt{Open Source Edition} de \RStudio ne gère pas les certificats \texttt{SSL}, il faut d'abord configurer le serveur \nginx correctement.

\par Nous allons nous servir de ce \href{https://docs.posit.co/ide/server-pro/}{guide} pour installer le \rstudio \textit{Open Source Edition}.
\subsection{Instalation de \jupyterhub}
\subsection{Instalation d'applications bio-informatiques}
\subsubsection{Instalation dans l'environnement isolé \biotools}
\subsubsection{Instalation dans un environnement isolé spécifique}
\section{Mise à jour du serveur}
\subsection{Mise à jour de la distribution \debian}
\subsection{Mise à jour de la distribution \conda}
\subsection{Mise à jour du langage \R}
\subsubsection{Mise à jour du langage}
\subsubsection{Mise à jour de \RStudio}
\subsection{Mise à jour de \jupyterhub}
\subsection{Mise à jour d'applications bio-informatiques}
\subsubsection{Mise à jour de l'environnement isolé \biotools}
\subsubsection{Mise à jour d'un environnement isolé spécifique}
\end{document}