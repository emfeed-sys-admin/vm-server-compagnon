# Mémo de première connexion sur cador-emfeed
Bonjour, 

vous venez d'arriver dans l'équipe EMFEED et voici la (mini) procédure pour se connecter 
à notre serveur « cador-emfeed » (cador par la suite).

Dans cette procédure, vous allez apprendre à :
- vous connectez une première fois ;
- vous connectez de façon pérenne et fluide par la suite ;
- vous connectez aux différents services de cador ;
- vous connectez au gestionnaire de version institutionnel (optionnel).

## Toute première fois
La première fois que vous allez vous connecter à cador va permettre au système `GNU/Linux Ubuntu` de créer votre dossier de travail sur le serveur et vous attribuer les droits d'écriture sur celui-ci. 
**Cette première connexion se fait uniquement via le protocole ssh.** 

### Sous windows
Sous windows, la procédure est différente si vous avez ou non les droits en administration sur votre station de travail (normalement non).
Il s'agira ici de :
- installer le logiciel « MobaXterm » ;
- se connecter au serveur cador-emfeed.

#### Je suis administrateur et c'est bien le fun
Il s'agira d'installer le logiciel « MobaXterm » version « Home Edition » en mode *installer edition* (la couleur verte).

Tout d'abord, rendez-vous sur la page de téléchargement de « MobaXterm » [ici](https://mobaxterm.mobatek.net/download-home-edition.html).
Ensuite, installez simplement le logiciel en vous laissant guider par la procédure, les choix par défaut devraient suffirent.

#### Je ne suis pas administrateur et c'est moins le fun
**Il faut en priorité demander à votre responsable d'installer la version « Home Edition » en mode *installer edition*.**
Si ce n'est pas possible, vous pouvez installer le logiciel « MobaXterm » version « Home Edition » en mode *portable edition* (la couleur bleue).

Tout d'abord, rendez-vous sur la page de téléchargement de « MobaXterm » [ici](https://mobaxterm.mobatek.net/download-home-edition.html).
Ensuite, installez simplement le logiciel dans un répertoire approprié (par exemple : `C:\\Documents\Applications\`) en vous laissant guider par la procédure, les choix par défaut devraient suffirent.

#### Se connecter au serveur cador
Pour de se connecter à cador, il faut vous munir de vos authentifiants (identifiant et mot de passe) LDAP Inrae que vous avez déjà du recevoir.
Il faut ensuite lancer « MobaXterm » et ouvrir une session ssh.

Vous avez déjà du recevoir un message de la Direction des Systèmes d'information (DSI) de l'INRAe de ce ![type](./img/ada-lovelace_iam-message-vr.png). 

### Sous GNU/Linux ou MacOS